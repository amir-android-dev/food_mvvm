package com.challenge3.food_mvvm.data.repository

import android.util.Log
import com.challenge3.food_mvvm.data.model.ResponseCategoriesList
import com.challenge3.food_mvvm.data.model.ResponseFoodsList
import com.challenge3.food_mvvm.data.server.ApiServices
import com.challenge3.food_mvvm.utils.MyResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response
import javax.inject.Inject

class FoodListRepository @Inject constructor(private val api: ApiServices) {

    suspend fun randomFood(): Flow<Response<ResponseFoodsList>> {
        return flow {
            emit(api.foodRandom())
        }.catch { e ->
            Log.e("randomFood", "No connection")
        }
            .flowOn(Dispatchers.IO)
    }

    suspend fun categoriesList(): Flow<MyResponse<ResponseCategoriesList>> {
        return flow {
            emit(MyResponse.loading())
            when (api.categoriesList().code()) {
                in 200..202 -> {
                    emit(MyResponse.success(api.categoriesList().body()))
                }
                422 -> {
                    emit(MyResponse.error(api.categoriesList().message()))
                }
                in 400..499 -> {
                    emit(MyResponse.error(api.categoriesList().message()))
                }
                in 500..599 -> {
                    emit(MyResponse.error(api.categoriesList().message()))
                }
            }
        }.catch { e ->
            emit(MyResponse.error(e.message.toString()))
        }
    }

    suspend fun foodList(letter: String): Flow<MyResponse<ResponseFoodsList>> {
        return flow {
            emit(MyResponse.loading())
            when (api.foodsList(letter).code()) {
                in 200..202 -> {
                    emit(MyResponse.success(api.foodsList(letter).body()))
                }
                422 -> {
                    emit(MyResponse.error(api.foodsList(letter).message()))
                }
                in 400..499 -> {
                    emit(MyResponse.error(api.foodsList(letter).message()))
                }
                in 500..599 -> {
                    emit(MyResponse.error(api.foodsList(letter).message()))
                }
            }
        }.catch { e ->
            emit(MyResponse.error(e.message.toString()))
        }
    }

    suspend fun foodsBySearch(letter: String): Flow<MyResponse<ResponseFoodsList>> {
        return flow {
            emit(MyResponse.loading())
            when (api.searchFood(letter).code()) {
                in 200..202 -> {
                    emit(MyResponse.success(api.searchFood(letter).body()))
                }
            }
        }.catch { emit(MyResponse.error(it.message.toString())) }
            .flowOn(Dispatchers.IO)
    }

    suspend fun foodsByCategory(letter: String): Flow<MyResponse<ResponseFoodsList>> {
        return flow {
            emit(MyResponse.loading())
            when (api.foodsByCategory(letter).code()) {
                in 200..202 -> {
                    emit(MyResponse.success(api.foodsByCategory(letter).body()))
                }
            }
        }.catch { emit(MyResponse.error(it.message.toString())) }
            .flowOn(Dispatchers.IO)
    }

}
