package com.challenge3.food_mvvm.data.repository

import com.challenge3.food_mvvm.data.database.FoodDao
import com.challenge3.food_mvvm.data.database.FoodEntity
import com.challenge3.food_mvvm.data.model.ResponseFoodsList
import com.challenge3.food_mvvm.data.server.ApiServices
import com.challenge3.food_mvvm.utils.MyResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class FoodDetailsRepository @Inject constructor(
    private val api: ApiServices,
    private val dao: FoodDao
) {
    suspend fun foodDetail(id: Int): Flow<MyResponse<ResponseFoodsList>> {

        return flow {
            emit(MyResponse.loading())
            when (api.foodDetail(id).code()) {
                in 200..202 -> {
                    emit(MyResponse.success(api.foodDetail(id).body()))
                }
                422 -> {
                    emit(MyResponse.error(api.foodDetail(id).message()))
                }
                in 400..499 -> {
                    emit(MyResponse.error(api.foodDetail(id).message()))
                }
                in 500..599 -> {
                    emit(MyResponse.error(api.foodDetail(id).message()))
                }
            }
        }.catch { e ->
            emit(MyResponse.error(e.message.toString()))
        }
    }

    suspend fun saveFood(entity: FoodEntity) = dao.saveFood(entity)
    suspend fun deleteFood(entity: FoodEntity) = dao.deleteFood(entity)
    fun isFoodExisted(id: Int) = dao.existsFood(id)
}
