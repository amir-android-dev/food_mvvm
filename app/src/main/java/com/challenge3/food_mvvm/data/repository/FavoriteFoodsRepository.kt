package com.challenge3.food_mvvm.data.repository

import com.challenge3.food_mvvm.data.database.FoodDao
import javax.inject.Inject

class FavoriteFoodsRepository @Inject constructor(private val dao: FoodDao) {


    fun getAllFoods() = dao.getAllFoods()
}