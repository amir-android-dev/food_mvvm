package com.challenge3.food_mvvm.ui.detail

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import coil.load
import com.challenge3.food_mvvm.R
import com.challenge3.food_mvvm.data.database.FoodEntity
import com.challenge3.food_mvvm.databinding.FragmentFoodDetailBinding
import com.challenge3.food_mvvm.ui.detail.player.PlayerActivity
import com.challenge3.food_mvvm.ui.list.FoodsListFragment
import com.challenge3.food_mvvm.utils.*
import com.challenge3.food_mvvm.viewmodel.FoodDetailViewModel
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import org.json.JSONObject
import javax.inject.Inject

@AndroidEntryPoint
class FoodDetailFragment : Fragment() {
    private lateinit var binding: FragmentFoodDetailBinding

    //other
    private val args: FoodDetailFragmentArgs by navArgs()
    private var foodId = 0
    private val viewModel: FoodDetailViewModel by viewModels()

    @Inject
    lateinit var entity: FoodEntity

    @Inject
    lateinit var connection: CheckConnection
    private var isFavorite = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentFoodDetailBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            //get data
            foodId = args.foodId
            //back
            detailBack.setOnClickListener { findNavController().navigateUp() }
            //detail api call
            viewModel.loadFoodDetail(foodId)
            viewModel.foodDetail.observe(viewLifecycleOwner) {
                when (it.status) {
                    MyResponse.Status.LOADING -> {
                        detailLoading.isVisible(true, detailContentLay)
                    }
                    MyResponse.Status.SUCCESS -> {
                        detailLoading.isVisible(false, detailContentLay)
                        it.data?.meals?.get(0)?.let { itMeal ->
                            //entity
                            entity.id = itMeal.idMeal!!.toInt()
                            entity.img = itMeal.strMealThumb!!
                            entity.title = itMeal.strMeal.toString()

                            //Set data
                            foodCoverImg.load(itMeal.strMealThumb) {
                                crossfade(true)
                                crossfade(500)
                            }
                            foodCategoryTxt.text = itMeal.strCategory
                            foodAreaTxt.text = itMeal.strArea
                            foodTitleTxt.text = itMeal.strMeal
                            foodDescTxt.text = itMeal.strInstructions
                            //Play
                            if (!itMeal.strYoutube.isNullOrEmpty()) {
                                foodPlayImg.visibility = View.VISIBLE
                                foodPlayImg.setOnClickListener {
                                    val videoId = itMeal.strYoutube.split("=")[1]
                                    Intent(requireContext(), PlayerActivity::class.java).also {
                                        it.putExtra(VIDEO_ID, videoId)
                                        startActivity(it)
                                    }
                                }
                            } else {
                                foodPlayImg.visibility = View.GONE
                            }
                            //Source
                            if (itMeal.strSource != null) {
                                foodSourceImg.visibility = View.VISIBLE
                                foodSourceImg.setOnClickListener {
                                    startActivity(
                                        Intent(
                                            Intent.ACTION_VIEW,
                                            Uri.parse(itMeal.strSource)
                                        )
                                    )
                                }
                            } else {
                                foodSourceImg.visibility = View.GONE
                            }
                            //Json Array
                            val jsonData = JSONObject(Gson().toJson(it.data))
                            val meals = jsonData.getJSONArray("meals")
                            val meal = meals.getJSONObject(0)
                            //Ingredient
                            for (i in 1..15) {
                                val ingredient = meal.getString("strIngredient$i")
                                if (ingredient.isNullOrEmpty().not()) {
                                    ingredientsTxt.append("$ingredient\n")
                                }
                            }
                            //Measure
                            for (i in 1..15) {
                                val measure = meal.getString("strMeasure$i")
                                if (measure.isNullOrEmpty().not()) {
                                    measureTxt.append("$measure\n")
                                }
                            }

                        }
                    }
                    MyResponse.Status.ERROR -> {
                        detailLoading.isVisible(false, homeDisLay)
                        displayToast(requireContext(), it.message.toString())

                    }
                }
            }
            //favorite
            viewModel.existesFood(foodId)
            viewModel.isFavoriteFoodExisted.observe(viewLifecycleOwner) {
                if (it) {
                    isFavorite = it
                    detailFav.setColorFilter(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.tartOrange
                        )
                    )
                } else {
                    detailFav.setColorFilter(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.black
                        )
                    )
                }
            }
            //save & delete
            detailFav.setOnClickListener {
                if (!isFavorite) {
                    viewModel.saveFood(entity)
                } else {
                    viewModel.deleteFood(entity)
                }
            }
            //internet
            connection.observe(viewLifecycleOwner) {
                if (it) {
                    checkConnectionOrEmpty(false, FoodsListFragment.PageState.NONE)
                } else {
                    checkConnectionOrEmpty(true, FoodsListFragment.PageState.NETWORK)
                }
            }
        }
    }

    //Internet


    private fun checkConnectionOrEmpty(isShownError: Boolean, state: FoodsListFragment.PageState) {
        binding.apply {
            if (isShownError) {
                homeDisLay.isVisible(true, detailContentLay)
                when (state) {
                    FoodsListFragment.PageState.EMPTY -> {
                        statusLay.disImg.setImageResource(R.drawable.box)
                        statusLay.disTxt.text = getString(R.string.emptyList)
                    }
                    FoodsListFragment.PageState.NETWORK -> {
                        statusLay.disImg.setImageResource(R.drawable.disconnect)
                        statusLay.disTxt.text = getString(R.string.checkInternet)
                    }
                    else -> {}
                }
            } else {
                homeDisLay.isVisible(false, detailContentLay)
            }
        }
    }
}
