package com.challenge3.food_mvvm.ui.detail.player

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import com.challenge3.food_mvvm.R
import com.challenge3.food_mvvm.databinding.ActivityPlayerBinding
import com.challenge3.food_mvvm.utils.VIDEO_ID
import com.challenge3.food_mvvm.utils.YOUTUBE_API_KEY
import com.challenge3.food_mvvm.utils.displayToast
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer

@Suppress("DEPRECATION")
class PlayerActivity : YouTubeBaseActivity() {
    private lateinit var binding: ActivityPlayerBinding

    //other
    private lateinit var player: YouTubePlayer

    //full screen
    private var videoId = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlayerBinding.inflate(layoutInflater)
        //full screen
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(binding.root)
        videoId = intent.getStringExtra(VIDEO_ID).toString()
        //player initialize
        val listener = object : YouTubePlayer.OnInitializedListener {
            override fun onInitializationSuccess(
                p0: YouTubePlayer.Provider?,
                p1: YouTubePlayer,
                p2: Boolean
            ) {
                player = p1
                player.loadVideo(videoId)
                player.play()
            }

            override fun onInitializationFailure(
                p0: YouTubePlayer.Provider?,
                p1: YouTubeInitializationResult
            ) {
                finish()
            }
        }
        binding.videoPlayer.initialize(YOUTUBE_API_KEY, listener)
    }

    override fun onDestroy() {
        super.onDestroy()
        player.release()
    }
}