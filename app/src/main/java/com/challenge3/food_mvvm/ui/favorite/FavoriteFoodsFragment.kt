package com.challenge3.food_mvvm.ui.favorite

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.challenge3.food_mvvm.R
import com.challenge3.food_mvvm.databinding.FragmentFavoriteFoodsBinding
import com.challenge3.food_mvvm.viewmodel.FavoriteFoodsViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FavoriteFoodsFragment : Fragment() {
    //binding
    lateinit var binding: FragmentFavoriteFoodsBinding

    //other
    private val viewModel: FavoriteFoodsViewModel by viewModels()

    @Inject
    lateinit var adapter: FavoriteAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentFavoriteFoodsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            viewModel.getAllFavorites()
            viewModel.favoriteFoods.observe(viewLifecycleOwner) {
                if (it.isEmpty()) {
                    emptyLay.visibility = View.VISIBLE
                    statusLay.disImg.setImageResource(R.drawable.box)
                    statusLay.disTxt.text = getString(R.string.emptyList)
                } else {
                    favoriteList.layoutManager = LinearLayoutManager(requireContext())
                    favoriteList.adapter = adapter
                    adapter.setData(it)

                    adapter.setOnItemClickListener { entity ->
                        val action =
                            FavoriteFoodsFragmentDirections.actionFoodsListFragmentToFoodDetailFragment(
                                entity.id
                            )
                        findNavController().navigate(action)

                    }
                }
            }
        }
    }

}