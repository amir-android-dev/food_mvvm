package com.challenge3.food_mvvm.ui.list

import android.graphics.Path.Direction
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.challenge3.food_mvvm.R
import com.challenge3.food_mvvm.databinding.FragmentFoodsListBinding
import com.challenge3.food_mvvm.ui.list.adapters.CategoriesAdapter
import com.challenge3.food_mvvm.ui.list.adapters.FoodsAdapter
import com.challenge3.food_mvvm.utils.*
import com.challenge3.food_mvvm.viewmodel.FoodsListViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FoodsListFragment : Fragment() {
    private lateinit var binding: FragmentFoodsListBinding

    //other
    private val viewModel: FoodsListViewModel by viewModels()

    @Inject
    lateinit var categoryAdapter: CategoriesAdapter

    @Inject
    lateinit var foodsAdapter: FoodsAdapter

    @Inject
    lateinit var connection: CheckConnection

    enum class PageState { EMPTY, NETWORK, NONE }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFoodsListBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            //random Food
           // viewModel.loadRandomFood()
            viewModel.randomFoodData.observe(viewLifecycleOwner) {

                it[0].let { meal ->
                    headerImg.load(meal.strMealThumb) {
                        crossfade(500)
                        crossfade(true)
                    }
                }
            }
            //filters
            viewModel.loadFilterList()
            viewModel.filtersList.observe(viewLifecycleOwner) {
                filterSpinner.setupListWithAdapter(it, callback = { letter ->
                    viewModel.loadFoods(letter)

                })
            }
            //category
           // viewModel.loadCategoriesList()
            viewModel.categoriesList.observe(viewLifecycleOwner) {
                when (it.status) {
                    MyResponse.Status.LOADING -> {
                        homeCategoryLoading.isVisible(true, categoryList)
                    }
                    MyResponse.Status.SUCCESS -> {
                        homeCategoryLoading.isVisible(false, categoryList)
                        categoryAdapter.setData(it.data!!.categories)
                        categoryList.setupRecyclerView(
                            LinearLayoutManager(
                                requireContext(),
                                LinearLayoutManager.HORIZONTAL,
                                false
                            ),
                            categoryAdapter
                        )
                    }
                    MyResponse.Status.ERROR -> {
                        homeCategoryLoading.isVisible(false, categoryList)
                        displayToast(requireContext(), it.message.toString())
                    }
                }

            }

            categoryAdapter.setOnItemClickListener {
                viewModel.loadFoodByCategory(it.strCategory.toString())
            }
            //Foods
            viewModel.loadFoods("A")
            viewModel.foodsList.observe(viewLifecycleOwner) {
                when (it.status) {
                    MyResponse.Status.LOADING -> {
                        homeFoodsLoading.isVisible(true, foodsList)
                    }
                    MyResponse.Status.SUCCESS -> {
                        homeFoodsLoading.isVisible(false, foodsList)
                        if (it.data!!.meals != null) {
                            if (it.data.meals!!.isNotEmpty()) {
                                checkConnection(false, PageState.NONE)
                                foodsAdapter.setData(it.data.meals)
                                foodsList.setupRecyclerView(
                                    LinearLayoutManager(
                                        requireContext(),
                                        LinearLayoutManager.HORIZONTAL,
                                        false
                                    ),
                                    foodsAdapter
                                )
                            }
                        } else {
                            checkConnection(true, PageState.EMPTY)
                        }
                    }
                    MyResponse.Status.ERROR -> {
                        homeFoodsLoading.isVisible(false, foodsList)
                        displayToast(requireContext(), it.message.toString())
                    }
                }
            }
            //clickListener food
            foodsAdapter.setOnItemClickListener {
                val action =
                    FoodsListFragmentDirections.actionFoodsListFragmentToFoodDetailFragment(it.idMeal!!.toInt())
                findNavController().navigate(action)
            }
            //search
            searchEdt.addTextChangedListener {
                if (it.toString().length > 2) {
                    viewModel.loadFoodBySearch(it.toString())
                }
            }

            //Internet
            connection.observe(viewLifecycleOwner) {
                if (it) {
                    checkConnection(false, PageState.NONE)
                } else {
                    checkConnection(true, PageState.NETWORK)
                }
            }
        }
    }

    private fun checkConnection(isConnected: Boolean, state: PageState) {
        binding.apply {
            if (isConnected) {
                homeDisLay.isVisible(true, homeContent)
                when (state) {
                    PageState.EMPTY -> {
                        statusLay2.disImg.setImageResource(R.drawable.box)
                        statusLay2.disTxt.text = getString(R.string.emptyList)
                    }
                    PageState.NETWORK -> {
                        headerImg.visibility = View.GONE
                        statusLay2.disImg.setImageResource(R.drawable.disconnect)
                        statusLay2.disTxt.text = getString(R.string.checkInternet)
                    }
                    else -> {}
                }
            } else {
                homeDisLay.isVisible(false, homeContent)
            }
        }
    }
}
//an issue