package com.challenge3.food_mvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge3.food_mvvm.data.database.FoodEntity
import com.challenge3.food_mvvm.data.model.ResponseFoodsList
import com.challenge3.food_mvvm.data.repository.FoodDetailsRepository
import com.challenge3.food_mvvm.utils.MyResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FoodDetailViewModel @Inject constructor(private val repository: FoodDetailsRepository) :
    ViewModel() {
    private val _foodDetail = MutableLiveData<MyResponse<ResponseFoodsList>>()
    val foodDetail: LiveData<MyResponse<ResponseFoodsList>>
        get() = _foodDetail

    fun loadFoodDetail(id: Int) = viewModelScope.launch(Dispatchers.IO) {
        repository.foodDetail(id).collect {
            _foodDetail.postValue(it)
        }
    }

    fun saveFood(entity: FoodEntity) = viewModelScope.launch {
        repository.saveFood(entity)
    }

    fun deleteFood(entity: FoodEntity) = viewModelScope.launch {
        repository.deleteFood(entity)
    }

    private val _isFavoriteFoodExisted = MutableLiveData<Boolean>()
    val isFavoriteFoodExisted: LiveData<Boolean>
        get() = _isFavoriteFoodExisted

    fun existesFood(id: Int) = viewModelScope.launch {
        repository.isFoodExisted(id).collect {
            _isFavoriteFoodExisted.postValue(it)
        }
    }
}