package com.challenge3.food_mvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge3.food_mvvm.data.database.FoodEntity
import com.challenge3.food_mvvm.data.repository.FavoriteFoodsRepository
import com.challenge3.food_mvvm.utils.MyResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoriteFoodsViewModel @Inject constructor(private val repository: FavoriteFoodsRepository) :
    ViewModel() {
    private val _favoriteFoods = MutableLiveData<List<FoodEntity>>()
    val favoriteFoods: LiveData<List<FoodEntity>>
        get() = _favoriteFoods

    fun getAllFavorites() =
        viewModelScope.launch {
            repository.getAllFoods().collect {
                _favoriteFoods.postValue(it)
            }
        }
}