package com.challenge3.food_mvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge3.food_mvvm.data.model.ResponseCategoriesList
import com.challenge3.food_mvvm.data.model.ResponseCategoriesList.Category
import com.challenge3.food_mvvm.data.model.ResponseFoodsList
import com.challenge3.food_mvvm.data.model.ResponseFoodsList.Meal
import com.challenge3.food_mvvm.data.repository.FoodListRepository
import com.challenge3.food_mvvm.utils.MyResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FoodsListViewModel @Inject constructor(private val repository: FoodListRepository) :
    ViewModel() {
    init {
        loadRandomFood()
        loadCategoriesList()
    }

    //random food
    private val _randomFoodData = MutableLiveData<List<Meal>>()
    val randomFoodData: LiveData<List<Meal>>
        get() = _randomFoodData

    fun loadRandomFood() = viewModelScope.launch(Dispatchers.IO) {
        repository.randomFood().collect {
            _randomFoodData.postValue(it.body()?.meals!!)
        }
    }

    //filter
    private val _filtersList = MutableLiveData<MutableList<Char>>()
    val filtersList: LiveData<MutableList<Char>>
        get() = _filtersList

    fun loadFilterList() = viewModelScope.launch(Dispatchers.IO) {
        val letters = listOf('A'..'Z').flatten().toMutableList()
        _filtersList.postValue(letters)
    }

    //category
    private val _categoriesList = MutableLiveData<MyResponse<ResponseCategoriesList>>()
    val categoriesList: LiveData<MyResponse<ResponseCategoriesList>>
        get() = _categoriesList

    fun loadCategoriesList() = viewModelScope.launch(Dispatchers.IO) {
        repository.categoriesList().collect {
            _categoriesList.postValue(it)
        }
    }

    //foods
    private val _foodsList = MutableLiveData<MyResponse<ResponseFoodsList>>()
    val foodsList: LiveData<MyResponse<ResponseFoodsList>>
        get() = _foodsList

    fun loadFoods(letter: String) = viewModelScope.launch(Dispatchers.IO) {
        repository.foodList(letter).collect {
            _foodsList.postValue(it)
        }
    }

    fun loadFoodBySearch(letter: String) = viewModelScope.launch(Dispatchers.IO) {
        repository.foodsBySearch(letter).collect {
            _foodsList.postValue(it)
        }
    }

    fun loadFoodByCategory(letter: String) = viewModelScope.launch(Dispatchers.IO) {
        repository.foodsByCategory(letter).collect {
            _foodsList.postValue(it)
        }
    }


}