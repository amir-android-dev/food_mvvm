package com.challenge3.food_mvvm.utils


import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import androidx.lifecycle.LiveData
import javax.inject.Inject


class CheckConnection @Inject constructor(private val cm: ConnectivityManager) : LiveData<Boolean>() {

    private val networkCallback = object : ConnectivityManager.NetworkCallback() {
        //if internet is available
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            postValue(true)
        }

        //if there is not internet
        override fun onLost(network: Network) {
            super.onLost(network)
            postValue(false)
        }
    }

    //to use cm, we have to register and unregister it.
    //when is active must be registered, when not it must be unregistered
    override fun onActive() {
        super.onActive()
        //for network request we need a manifest permission
        val networkRequest = NetworkRequest.Builder().build()
        cm.registerNetworkCallback(networkRequest, networkCallback)
    }

    override fun onInactive() {
        super.onInactive()
        cm.unregisterNetworkCallback(networkCallback)

    }
}
