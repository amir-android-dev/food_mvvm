package com.challenge3.food_mvvm.utils.di

import android.content.Context
import androidx.room.Room
import com.challenge3.food_mvvm.data.database.FoodDatabase
import com.challenge3.food_mvvm.data.database.FoodEntity
import com.challenge3.food_mvvm.utils.FOOD_DB_DATABASE
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DbModule {

    @Provides
    @Singleton
    fun dao(database: FoodDatabase) = database.foodDao()

    @Provides
    @Singleton
    fun entity() = FoodEntity()

    @Provides
    @Singleton
    fun room(@ApplicationContext context: Context) = Room.databaseBuilder(
        context, FoodDatabase::class.java, FOOD_DB_DATABASE
    )
        .allowMainThreadQueries()
        .fallbackToDestructiveMigration()
        .build()
}